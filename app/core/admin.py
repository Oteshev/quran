from django.contrib import admin
from core.models import Surah, Ayat


admin.site.register(Surah)
admin.site.register(Ayat)
